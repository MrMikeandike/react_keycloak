import React from 'react';
import Keycloak from 'keycloak-js'
import {KeycloakProvider} from 'react-keycloak'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import Welcome from './Welcome';
import Secured from './Secured';
import Logout from './Logout';

const keycloak = new Keycloak('/keycloak.json')
const initconf = {
 // onLoad: 'login-required',
  
}
const App = () => {
  
  
  return (
    <KeycloakProvider keycloak={keycloak} initConfig={initconf} >
        <Router>
          <h1>web page default header</h1>
          <Link to='/'><p>public page</p></Link>
          <Link to='/secure'><p>Secure Page</p></Link>
          <Route exact path='/' component={Welcome}/>
          <Route exact path ='/logout' component={Logout}/>
          <Route exact path='/secure' component={Secured}/>

          
        </Router>
    </KeycloakProvider>
  );
}

export default App;

