import React from 'react'

const Logout = () => {
    return (
        <div className='logout'>
            <p>Logout successful</p>
        </div>
    )
}

export default Logout
