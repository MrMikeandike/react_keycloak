import React from 'react'
import {useKeycloak} from 'react-keycloak'

const Secured = () => {
    const [keycloak, initialized ] = useKeycloak()
    return (
        <div>
            <div>
                {`User is ${!keycloak.authenticated ? 'NOT ' : ''}authenticated`}
                
            </div>
            {!keycloak.authenticated ?
                <div>
                    <button onClick={() => {keycloak.login()}}>Login</button>
                </div>
                :
                <div>
                    <button onClick={() => {keycloak.logout({redirectUri: 'http://localhost:3000'})}}>Logout</button>
                </div>
                }
        </div>
    )
}

export default Secured
